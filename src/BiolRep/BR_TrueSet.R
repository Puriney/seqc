comparisons <- c('GM12892_vs_H1-hESC', "2013-04-17")
## comparisons <- c('H1-hESC_vs_MCF-7', "2013-04-17")
## comparisons <- c('GM12892_vs_MCF-7', "2013-04-17")
load(paste("../data/BiolRep/Results_",comparisons[1],"_",comparisons[2],".Rdata", sep=''))

deseq <- results.full[["DESeq"]]$de
edger <- results.full[["edgeR"]]$de$table
poissSeq <- results.full[["PoissonSeq"]]$res
limmaqn <- results.full[["limmaQN"]]$tab
limmaVoom <- results.full[["limmaVoom"]]$tab
cuffdiff <- read.table(paste("/scratchBulk/dob2014/SEQC/ENCODE/Mapped_Caltech/cuffDiff/",
                             comparisons[1],"/gene_exp.diff", sep=''),
                       stringsAsFactors=FALSE,
                      sep='\t', header=TRUE, row.names=2)

bayseq <- merge(results.full[['baySeq']]$MA,
                    results.full[['baySeq']]$de[,c("Likelihood","FDR")],
                    by.x='row.names', by.y='row.names')
rownames(bayseq) <- bayseq[,1]
bayseq <- bayseq[,-1]
bayseq[,'M'] <- -bayseq[,'M'] ## invert M values to be conssitant with other methods

## subset by logFC ==1 and adj-pval<= 0.05
pco <- 0.05 ## adj p-val cutoff
lfc <- 1 ## absolute logFC cutoff
deseq <- deseq[deseq$padj <= pco & abs(deseq$log2FoldChange) >= lfc ,]
edger <- edger[edger$FDR <= pco & abs(edger$logFC) >= lfc ,]
poissSeq <- poissSeq[poissSeq$fdr <= pco & abs(poissSeq$log.fc) >= lfc , ]
limmaqn <-  limmaqn[limmaqn[,"adj.P.Val"] <= pco & abs(limmaqn[,"logFC"]) >= lfc , ]
limmaVoom <-  limmaVoom[limmaVoom[,"adj.P.Val"] <= pco & abs(limmaVoom[,"logFC"]) >= lfc , ]
cuffdiff <- cuffdiff[cuffdiff$q_value <= pco & abs(cuffdiff$`log2.fold_change.`) >= lfc, ]
bayseq <- bayseq[bayseq$FDR <= pco & abs(bayseq$M) >= lfc, ]


## intersect all names
intersect.names <- intersect(deseq$id, intersect(rownames(edger),
                                   intersect(rownames(poissSeq),
                                         intersect(rownames(limmaqn),
                                               intersect(rownames(limmaVoom),
                                                     intersect(rownames(cuffdiff), rownames(bayseq)))))))

## plot overlaps
all.names <- list(DESeq=deseq$id, edgeR=rownames(edger),
                  PoissonSeq=rownames(poissSeq), limmaQN=rownames(limmaqn),
                  limmaVoom=rownames(limmaVoom), Cuffdiff=rownames(cuffdiff),
                  baySeq=rownames(bayseq), intersection.genes=intersect.names)

## save the list so can be plotted using Vennerable
save(all.names, file=paste("../data/BiolRep/",comparisons[1],"_", Sys.Date(),"_trueset.Rdata", sep=''))

## Run the following code in the R version that support Vennerable R<2.13
## load("../data/trueset.Rdata")
## require("Vennerable")
## vn <- Venn(all.names)
## plot(vn, type="ChowRuskey")
