##########################
## Collect HTSeq output
## and generate count matrix
##########################

data.files <- dir("/scratchBulk/dob2014/SEQC/ENCODE/Mapped_Caltech/tophat", pattern='*', full.names=T)

first.file <- read.table(paste(data.files[1], "/accepted_hits.bam_queryname_sorted.htseq_count", sep=''),
                         stringsAsFactors=FALSE, row.names=1)

BR.counts <- matrix(0, ncol=length(data.files),
                    nrow=nrow(first.file))
rownames(BR.counts) <- rownames(first.file)

for(i in 1:length(data.files)){
  
  BR.counts[,i] <- read.table(paste(data.files[i], "/accepted_hits.bam_queryname_sorted.htseq_count", sep=''),
                    stringsAsFactors=FALSE,
                    header=FALSE, row.names=1)[,1]

}  


colnames(BR.counts) <- gsub("^s_", '',basename(data.files))

## remove 'special counters added by HTseq
BR.counts <- BR.counts[grep('no_feature|ambiguous|too_low_aQual|not_aligned|alignment_not_unique' ,
                                 rownames(BR.counts), invert=TRUE),]

## remove 'V2' from sample name
colnames(BR.counts) <- gsub('_V2', '',colnames(BR.counts))

## save
save(BR.counts, file="../data/BiolRep/BR_HTSeq.Rdata")
