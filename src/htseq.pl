# htseq.pl
# $dir is the project directory
# $gtf is the path of the gtf file

($dir,$gtf) = @ARGV;
opendir DIR, "${dir}";
@files = readdir DIR;
closedir DIR;
@files = grep(/\.sam/, @files);
foreach $file (@files)
{
	$name = substr($file,0,-4);
	print "Counting ${name}\n";

	`htseq-count -m intersection-strict -s no ${dir}/${name}.sam $gtf -t exon > ${dir}/$name\.count`;
}
