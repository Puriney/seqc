## Functions for collecting and organizing data from Null model comparison

TrellisData <- function(geq, res, pval.index){
  ## Create a data frame of [p.val, quantile, method]
  ## that can be used for histogram function
  ## geq = 'gene.expression.quantile' generated by ExpressionQuantile
  ## res = list of DE methods output
  ## pval.index= list with same names as 'res' with elements method=p.val column index
  
  trellis.dat <- data.frame()
  for(j in 1:length(res)){
    ## collect the pval for every expression quantile
    method.dat <- lapply(seq(length(geq)),
                    function(i) data.frame(p.val=res[[j]][geq[[i]], pval.index[[names(res)[j]]] ],
                                           adj.pval=res[[j]][geq[[i]], pval.index[[names(res)[j]]]+1 ],
                                           quantile = names(geq)[i],stringsAsFactors=FALSE,
                                           method = rep(names(res)[j],length(geq[[i]]))
                                           ))
    
    method.dat <- as.data.frame(do.call(rbind, method.dat))
    trellis.dat <- rbind(trellis.dat, method.dat)
    }
  return(trellis.dat)
}

CollectWithinConditionDEData <- function(comparison, counts.dat, pval.index){
  ## Collect the DE data from a specified contrast (e.g. A12_A34)
  ## comparison = a string describing the comparison (e.g. A12_A34)
  ## counts.dat = gene count matrix
  ## Requires the results of DEanalysisHTseqTopHat2.R and cuffdiff
  ## return a list of DE results and a list of genes in each quantile
  
  files <- dir("../data/", pattern=paste(comparison, '[^0-9]', sep=''))
  if(length(files) >2){
    stop("Too many files. Need to specifiy only one R output and one cuffdiff file")
  } else if( length(files)==0){
    stop(paste(comparison, "comparison does not exist"))
  }

  ## Load DE results 'results.full'
  load(paste("../data/", files[grep("Rdata", files)], sep=''))

  ## load cuffdiff
  ResCuff <- read.table(paste("../data/",files[grep("txt", files)], sep=''), stringsAsFactors=FALSE,
                        sep='\t', header=TRUE, row.names=2)

  ## list of DE tables
  res <- list()
  res["DESeq"] <- list(results.full[["DESeq"]]$de) # DESeq
  res[["DESeq"]] <- res[["DESeq"]][is.finite(res[[1]][,8]),] ## remove all zero counts genes

  res["edgeR"] <- list(results.full[["edgeR"]]$de$table) # edgeR
  
  if(any(grepl('limma', names(results.full)))){
    res["limmaQN"] <- list(results.full[["limmaQN"]]$tab) # limmaQN
    res["limmaVoom"] <- list(results.full[["limmaVoom"]]$tab) # limmaVoom
  }
  res["PoissonSeq"] <- list(results.full[["PoissonSeq"]]$res) # PoissonSeq
  res["CuffDiff"] <- list(ResCuff) ## CuffDiff

  ## Bayseq does not report p-values
  ## therefore, duplicate FDR column and drop other columns
  ## res["baySeq"] <- list(results.full[["baySeq"]]$de[,c("Likelihood","FDR","FDR")])
  
  ## add rownames to DESeq and Poissonseq
  rownames(res[["DESeq"]]) <- res[["DESeq"]][,"id"]
  rownames(res[["PoissonSeq"]]) <- res[["PoissonSeq"]][,"gname"]

  ## generate gene lists by expression quantiles
  ## split the cond.name to sample names
  temp <- strsplit(unlist(strsplit(comparison, "_")), '')
  groupA <- sapply(seq(2,length(temp[[1]])), function(i) paste(temp[[1]][1], temp[[1]][i], sep='_'))
  groupB <- sapply(seq(2,length(temp[[2]])), function(i) paste(temp[[2]][1], temp[[2]][i], sep='_'))

  ## quantile genes by their mean expression values
  gene.means <- rowMeans(counts.dat[,c(groupA, groupB)])
  quant.values <- quantile(gene.means, probs=seq(0,1,0.25))

  ## Collect gene names in expression quantiles
  ## Note that this loop excludes genes with zero mean
  gene.list <- lapply((2:length(quant.values)), function(i)
                      names(gene.means[gene.means > quant.values[i-1] & gene.means <= quant.values[i] ]))
  names(gene.list) <- names(quant.values)[2:length(quant.values)]

  ## Make a data frame of pval, adjusted.pval, method, expression quantile
  ## this is used for trellis plotting
  pval.data <- TrellisData(gene.list, res, pval.index)
  return(list(res=res,pval.dat =pval.data))
}
